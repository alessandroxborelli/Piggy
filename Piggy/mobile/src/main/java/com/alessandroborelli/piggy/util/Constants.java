package com.alessandroborelli.piggy.util;

/**
 * Created by alessandroborelli on 13/07/16.
 */

public class Constants {

    public static final String TAG = "PIGGY";

    public static final String FIREBASE_APP = "piggy-889ec";
    public static final String FIREBASE_URL = "https://"+FIREBASE_APP+".firebaseio.com/";
    public static final String FIREBASE_USERS_URL = "https://"+FIREBASE_APP+".firebaseio.com/users/";
    public static final String FIREBASE_PIGGIES_URL = "https://"+FIREBASE_APP+".firebaseio.com/piggies/";
    public static final String FIREBASE_MOVEMENTS_URL = "https://"+FIREBASE_APP+".firebaseio.com/movements/";

    public static final String EMPTY_STRING = "";
    public static final int INDEX_NOT_FOUND = -1;

    public static final int ERROR_AUTH_EMAIL_NOT_VALID = 1001;
    public static final int ERROR_AUTH_PASSWORD_NOT_VALID = 1002;
    public static final int ERROR_AUTH_FIRST_NAME_NOT_VALID = 1003;
    public static final int ERROR_AUTH_LAST_NAME_NOT_VALID = 1004;

    public static final int REQUEST_CODE_GOOGLE_SIGN_IN = 1111;

    public static final int ANIM_DURATION_DROP_DOWN = 180;
    public static final int ANIM_DURATION_MOVEMENT_FVIEW = 250;

    public static String getFirebaseMovementsUrl(String currentPiggyID) {
        return FIREBASE_MOVEMENTS_URL+currentPiggyID+"/";
    }

    public static String getFirebasePiggiesUrl(String currentUserID) {
        return FIREBASE_USERS_URL+currentUserID+"/piggies";
    }
}
