package com.alessandroborelli.piggy.util;

import java.math.BigDecimal;

/**
 * Created by alessandroborelli on 30/10/2016.
 */

public class NumberUtils {

    public static double getScaledDouble(double aDouble) {
        BigDecimal bd = new BigDecimal(Double.toString(aDouble));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

}
