package com.alessandroborelli.piggy.presentation.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.alessandroborelli.piggy.util.AppSettings;
import com.alessandroborelli.piggy.util.BundleKeys;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.Notice;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by alessandroborelli on 13/07/16.
 */

public class LauncherActivity extends FirebaseActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check if is the first time
        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getBoolean(AppSettings.IS_FIRST_TIME)) {
            Log.d(Constants.TAG, getClass().getSimpleName() + "LauncherActivity first");
            Intent intent = new Intent(this, AuthenticationActivity.class);
            startActivity(intent);
            finish();
        } else {

            // Improve the startup time check if the user is already authenticated
            if (mAuthUser != null) {
                // User is signed in
                // Go to MainActivity
                Notice.debug(getClass(), " -> onCreate() -> already signed in:" + mAuthUser.getUid());
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(BundleKeys.IS_USER_SIGNED_IN, true);
                startActivity(intent);
                finish();
            } else {

                // If the mAuthUser is null you have to authenticate again and the
                Notice.debug(getClass(), " -> onCreate() -> user null!");
            }
        }

    }
/*
        // Check if is the first time
        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getBoolean(AppSettings.IS_FIRST_TIME)) {
            Intent intent = new Intent(this, AuthenticationActivity.class);
            startActivity(intent);
            finish();
        } else {

            // Improve the startup time check if the user is authenticated
            mAuth = FirebaseAuth.getInstance();

            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                // User is signed in
                // Go to MainActivity
                Log.d(Constants.TAG, getClass().getSimpleName()+"LauncherActivity signed_in:" + user.getUid());
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(BundleKeys.IS_USER_SIGNED_IN, true);
                startActivity(intent);
                finish();
            } else {

                //
                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null) {
                            // User is signed in
                            Log.d(Constants.TAG, getClass().getSimpleName()+"LauncherActivity  onAuthStateChanged:signed_in:" + user.getUid());
                            Intent intent = new Intent(LauncherActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // User is signed out
                            Log.d(Constants.TAG, getClass().getSimpleName()+"onAuthStateChanged:signed_out");
                            Intent intent = new Intent(LauncherActivity.this, AuthenticationActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                };

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuthListener != null) mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) mAuth.removeAuthStateListener(mAuthListener);
    }
*/
}
