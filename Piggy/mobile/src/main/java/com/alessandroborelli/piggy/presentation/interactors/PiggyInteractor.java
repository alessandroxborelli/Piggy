package com.alessandroborelli.piggy.presentation.interactors;

import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.util.Constants;
import com.google.firebase.database.FirebaseDatabase;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alessandroborelli on 23/10/2016.
 */

public class PiggyInteractor {

    public static void add(Piggy aPiggy, String uid) {
        String key = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_PIGGIES_URL).push().getKey();
        aPiggy.setCreatedBy(uid);
        Map<String, Object> piggyValues = aPiggy.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/piggies/" + key, piggyValues);
        childUpdates.put("/users/" + uid + "/piggies/" + key, true);

        FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);
    }

    public static void removeUserPiggy(String piggyKey, String uid) {
        FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_USERS_URL).child(uid).child("piggies").child(piggyKey).removeValue();
    }

}
