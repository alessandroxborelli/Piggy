package com.alessandroborelli.piggy.presentation.views.custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.util.Constants;

/**
 * Created by aborelli on 07/09/2016.
 *
 * Movements Floating View
 */
public class NewMovementFView extends FrameLayout implements View.OnClickListener, NumbersKeypad.OnKeypadClickListener {

    public static String CURRENCY = "£";
    public static String CURRENT_EDIT_TEXT_HEAD = CURRENCY;

    private OnMovementsFViewListener mListener;

    private boolean mIsVisible = false;
    private FloatingActionButton mFab;
    private LinearLayout mOutside;
    private LinearLayout mMainContainer;
    private ImageButton mAddIButton;
    private TextView mIncomeTView, mExpenseTView;
    private EditText mAmountEText;
    private NumbersKeypad mKeypad;


    public NewMovementFView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Inflate the layout
        LayoutInflater.from(context).inflate(R.layout.view_movements, this, true);

        mMainContainer = (LinearLayout) findViewById(R.id.movements_fview_container);
        mOutside = (LinearLayout) findViewById(R.id.movements_fview_outside);
        mOutside.setOnClickListener(this);

        mAddIButton = (ImageButton) findViewById(R.id.movements_fview_add_BT);
        mAddIButton.setOnClickListener(this);

        mIncomeTView = (TextView) findViewById(R.id.movements_fview_income_BT);
        mIncomeTView.setOnClickListener(this);
        mExpenseTView = (TextView) findViewById(R.id.movements_fview_expense_BT);
        mExpenseTView.setOnClickListener(this);

        mAmountEText = (EditText)findViewById(R.id.movements_fview_amount_ET);
        mAmountEText.setFocusable(false);
        mAmountEText.setClickable(true);
        mKeypad = (NumbersKeypad)findViewById(R.id.movements_fview_keypad);
        mKeypad.setOnKeypadListener(this);

        resetUI();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == mOutside.getId()){
            // Click outside of the main view so dismiss it
            dismiss();
            if(mListener != null) mListener.onDismiss();
        } else if (id == mAddIButton.getId()) {
            if(mListener != null) mListener.onAddClick();
        } else if (id == mIncomeTView.getId()) {
            onIncomeClick();
        } else if (id == mExpenseTView.getId()) {
            onExpenseClick();
        }

    }

    public void onIncomeClick() {
        String text = "";
        if(mAmountEText.getText().toString().length() > CURRENT_EDIT_TEXT_HEAD.length()) text = mAmountEText.getText().toString().substring(CURRENT_EDIT_TEXT_HEAD.length());
        CURRENT_EDIT_TEXT_HEAD = CURRENCY;
        mAmountEText.setText(NewMovementFView.CURRENT_EDIT_TEXT_HEAD+text);
        mIncomeTView.setTextColor(getResources().getColor(R.color.movements_fview_text_enable));
        mExpenseTView.setTextColor(getResources().getColor(R.color.movements_fview_text_disable));
    }

    public void onExpenseClick() {
        String text = "";
        if(mAmountEText.getText().toString().length() > CURRENT_EDIT_TEXT_HEAD.length()) text = mAmountEText.getText().toString().substring(CURRENT_EDIT_TEXT_HEAD.length());
        CURRENT_EDIT_TEXT_HEAD = "-"+CURRENCY;
        mAmountEText.setText(NewMovementFView.CURRENT_EDIT_TEXT_HEAD+text);
        mExpenseTView.setTextColor(getResources().getColor(R.color.movements_fview_text_enable));
        mIncomeTView.setTextColor(getResources().getColor(R.color.movements_fview_text_disable));
    }

    public void setAnchorFAB(FloatingActionButton aFab) {
        mFab = aFab;
    }

    public void resetUI() {
        onExpenseClick();
        mAddIButton.setEnabled(false);
        mAddIButton.setImageAlpha(130); //TODO put in a constant
        mAmountEText.setText(CURRENT_EDIT_TEXT_HEAD);
        mKeypad.resetUI();
    }

    public boolean isVisible() { return mIsVisible; }

    public void show() {
        mIsVisible = true;
        mOutside.setVisibility(View.VISIBLE);
        circularRevealDialog();
    }

    public void dismiss() {
        mIsVisible = false;
        mOutside.setVisibility(View.GONE);
        circularHideDialog();
        mFab.show();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void circularRevealDialog() {
        // make the view visible and start the animation
        mMainContainer.setVisibility(View.VISIBLE);
        int cx = mMainContainer.getWidth() - (mFab.getWidth()/2);
        int cy = mMainContainer.getHeight() - (mFab.getHeight()/2);
        float finalRadius = Math.max(mMainContainer.getWidth(), mMainContainer.getHeight());

        // create the animator for this view (the start radius is zero)
        final Animator circularReveal = ViewAnimationUtils.createCircularReveal(mMainContainer, cx, cy, 0, finalRadius);
        circularReveal.setDuration(Constants.ANIM_DURATION_MOVEMENT_FVIEW);
        circularReveal.setInterpolator(new DecelerateInterpolator());
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.d("db", "END ANIMATION OPEN");
            }
        });
        circularReveal.start();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void circularHideDialog() {
        // make the view visible and start the animation
        mMainContainer.setVisibility(View.VISIBLE);
        int cx = mMainContainer.getWidth() - (mFab.getWidth()/2);
        int cy = mMainContainer.getHeight() - (mFab.getHeight()/2);
        float finalRadius = Math.max(mMainContainer.getWidth(), mMainContainer.getHeight());

        // create the animator for this view (the start radius is zero)
        final Animator circularReveal = ViewAnimationUtils.createCircularReveal(mMainContainer, cx, cy, finalRadius, 0);
        circularReveal.setDuration(Constants.ANIM_DURATION_MOVEMENT_FVIEW);
        circularReveal.setInterpolator(new DecelerateInterpolator());
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.d("db", "END ANIMATION CLOSE");
                mMainContainer.setVisibility(View.GONE);
                resetUI();
            }
        });
        circularReveal.start();
    }

    public void setListener(OnMovementsFViewListener listener) {
        mListener = listener;
    }

    @Override
    public void onUpdateText(String text, boolean isTextValid) {
        if (isTextValid) {
            mAddIButton.setEnabled(true);
            mAddIButton.setImageAlpha(255);
        } else {
            mAddIButton.setEnabled(false);
            mAddIButton.setImageAlpha(130);
        }
        mAmountEText.setText(NewMovementFView.CURRENT_EDIT_TEXT_HEAD+text);
    }


    /**
     * OnKeypadClickListener
     */


    public interface OnMovementsFViewListener {
        void onCategoryClick();
        void onAddClick();
        void onShow();
        void onDismiss();
    }

}
