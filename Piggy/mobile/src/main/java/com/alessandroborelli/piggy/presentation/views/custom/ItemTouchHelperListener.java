package com.alessandroborelli.piggy.presentation.views.custom;

/**
 * Created by alessandroborelli on 16/09/16.
 */

public interface ItemTouchHelperListener {
    void onItemDismiss(int position);
}

