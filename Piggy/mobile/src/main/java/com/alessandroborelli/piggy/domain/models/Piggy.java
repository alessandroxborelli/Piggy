package com.alessandroborelli.piggy.domain.models;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.Exclude;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aborelli on 26/08/2016.
 */
public class Piggy {

    // Firebase data fields
    public long timestamp;
    public String createdBy;
    public int type;
    public String name;
    public String currency;
    public double income;

    private String mKey;

    // Icon
    private Bitmap mIconBitmap;
    private Drawable mIconDrawable;
    private int mIconResource;
    private Uri mIconUri;


    public Piggy(){
        timestamp = new Date().getTime();
        type = TYPE.PERSONAL;
        currency = "£";
        income = 0;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public Piggy setCreatedBy(String uid) {
        createdBy = uid;
        return this;
    }

    public Piggy setName(String name) {
        this.name = name;
        return this;
    }

    public Piggy setType(int aType) {
        type = aType;
        return this;
    }

    public Piggy setIncome(float value) {
        income = value;
        return this;
    }

    public Piggy setCurrency(String aCurrency) {
        currency = aCurrency;
        return this;
    }
    public Piggy setIconResource(@DrawableRes int mIconResource) {
        this.mIconResource = mIconResource;
        return this;
    }

    public Piggy setIconUri(Uri mIconUri) {
        this.mIconUri = mIconUri;
        return this;
    }

    public String getKey() {
        return mKey;
    }

    public int getType() { return type; }

    public String getName() {
        return name;
    }

    public double getIncome() { return income; }

    public String getCurrency() { return currency; }

    public String getDisplayableIncome() { return currency+income; }

    public boolean isMainPiggy() { return type == TYPE.MAIN; }


    public void applyName(TextView tv){
        tv.setText(name);
    }

    public void applyIncome(TextView tv){
        tv.setText(getDisplayableIncome());
    }

    public void applyIcon(ImageView iv){
        iv.setImageResource(mIconResource);
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("currency", currency);
        result.put("income", income);
        result.put("timestamp", timestamp);
        result.put("createdBy", createdBy);
        result.put("type", type);
        return result;
    }

    public static class TYPE {
        public static int MAIN = 1;
        public static int PERSONAL = 2;
        public static int SHARED = 3;
    }
}
