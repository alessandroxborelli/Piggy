package com.alessandroborelli.piggy.presentation.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.presentation.views.custom.NavigationHeaderView;
import com.alessandroborelli.piggy.util.Constants;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aborelli on 25/08/2016.
 */
public class PiggyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_PIGGY = 0;
    private static final int VIEW_TYPE_HEADER = 1;

    private ArrayList<Piggy> mPiggies;

    private NavigationHeaderView mHeader;

    private final View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mHeader.handlePiggyClick((Integer) view.getTag());
        }
    };

    public PiggyAdapter(ArrayList<Piggy> piggies, NavigationHeaderView header){
        mPiggies = piggies;
        // Add a null item a initial position to keep an empty space for the header view
        //mPiggies.add(0,null);
        mHeader = header;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_PIGGY:
                PiggyViewHolder holder = new PiggyViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_piggy_item, parent, false));
                holder.itemView.setOnClickListener(mClickListener);
                return holder;
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(mHeader);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PiggyViewHolder) {
            final PiggyViewHolder piggyViewHolder = (PiggyViewHolder) holder;
            holder.itemView.setTag(position);
            if (position < getListCount()) {
                Piggy piggy = mPiggies.get(position);
                piggy.applyIcon(piggyViewHolder.iconView);
                piggy.applyName(piggyViewHolder.nameTextView);
                piggy.applyIncome(piggyViewHolder.incomeTextView);
                piggyViewHolder.moreView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mHeader.handlePiggyOptionClick(piggyViewHolder.moreView, (Integer) holder.itemView.getTag());
                    }
                });
            } else if (position == getListCount()) {
                // Add account item
                piggyViewHolder.iconView.setImageResource(R.drawable.ic_add);
                piggyViewHolder.nameTextView.setText("Add new piggy");
                piggyViewHolder.moreView.setVisibility(View.GONE);
                piggyViewHolder.incomeTextView.setVisibility(View.GONE);
                piggyViewHolder.membersLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        // Size of the piggies list and add button at the end
        return mPiggies.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? VIEW_TYPE_HEADER : VIEW_TYPE_PIGGY;
    }

    public int getListCount() {
        return mPiggies.size();
    }

    public Piggy get(int position) {
        return mPiggies.get(position);
    }

    public boolean add(Piggy aPiggy) {
        int index = indexOfByKey(aPiggy.getKey());
        if (index == Constants.INDEX_NOT_FOUND) {
            mPiggies.add(aPiggy);
            notifyItemInserted(mPiggies.size()-1);
            return true;
        } else {
            update(aPiggy, index);
        }
        return false;
    }

    public void addAll(Piggy... piggies) {
        mPiggies.addAll(Arrays.asList(piggies));
    }

    public boolean remove(String key) {
        int index = indexOfByKey(key);
        if (index != Constants.INDEX_NOT_FOUND) {
            mPiggies.remove(index);
            notifyItemRemoved(index);
            return true;
        }
        return false;
    }

    public void move(Piggy aPiggy, int position) {
        int index = mPiggies.indexOf(aPiggy);
        mPiggies.remove(index);
        mPiggies.add(position, aPiggy);
    }

    public void update(Piggy aPiggy, int position) {
        mPiggies.set(position, aPiggy);
        notifyDataSetChanged();
    }

    public void insert(Piggy aPiggy, int position) {
        mPiggies.add(position, aPiggy);
    }

    public void clear(){
        mPiggies.clear();
    }

    public int indexOfByKey(String key) {
        if (mPiggies != null && mPiggies.size() > 0) {
            for (int i=0; i < mPiggies.size(); i++) {
                Piggy piggy = mPiggies.get(i);
                if (piggy != null && piggy.getKey() != null && piggy.getKey().equals(key)) return i;
            }
        }
        return Constants.INDEX_NOT_FOUND;
    }

    private static class PiggyViewHolder extends RecyclerView.ViewHolder {
        ImageView iconView, moreView;
        TextView nameTextView, incomeTextView;
        LinearLayout membersLayout;

        public PiggyViewHolder(View itemView) {
            super(itemView);
            iconView = (ImageView) itemView.findViewById(R.id.piggy_adapter_item_icon);
            moreView = (ImageView) itemView.findViewById(R.id.piggy_adapter_item_more);
            nameTextView = (TextView) itemView.findViewById(R.id.piggy_adapter_item_name);
            incomeTextView = (TextView) itemView.findViewById(R.id.piggy_adapter_item_income);
            membersLayout = (LinearLayout) itemView.findViewById(R.id.piggy_adapter_item_members);
        }
    }

    private static class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
