package com.alessandroborelli.piggy.presentation.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.domain.models.User;
import com.alessandroborelli.piggy.presentation.interactors.PiggyInteractor;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.Notice;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by alessandroborelli on 10/10/2016.
 */

public class PiggyActivity extends FirebaseActivity{

    private static boolean IS_FIRST_LOAD = true;

    protected User mUser;
    protected DatabaseReference mUserRef;
    protected DatabaseReference mPiggiesRef;
    private Piggy mCurrentPiggy;
    private ValueEventListener mUserRefListener;
    private ChildEventListener mPiggyRefListener;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Current db user
        //mUser = getUser();

        // Piggies database ref
        //mPiggiesRef = getFirebaseDatabase().getReferenceFromUrl(Constants.getFirebasePiggiesUrl(mAuthUser.getUid()));
    }

    @Override
    public void onStart() {
        super.onStart();

        mUser = getUser();
        // Add value event listener to the post
        // [START post_value_event_listener]
        /*
        ChildEventListener piggyRefListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Notice.debug(getClass(), "Piggy onAdded ");
                // Get Piggy object and use the values to update the UI
                String piggyKey = dataSnapshot.getKey();

                getFirebaseDatabase().getReference().child("piggies/" + piggyKey).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        Piggy piggy = snapshot.getValue(Piggy.class);
                        if (piggy != null) {
                            piggy.setKey(snapshot.getKey());
                            Notice.debug(getClass(), "Piggy added " + piggy.name + " key: " + snapshot.getKey());
                            onPiggyAdded(piggy);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Notice.debug(getClass(), "Piggy cancelled ");
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Notice.debug(getClass(), "Piggy onChanged ");
                Piggy piggy = dataSnapshot.getValue(Piggy.class);
                if (piggy != null) {
                    piggy.setKey(dataSnapshot.getKey());
                    Notice.debug(getClass(), "Piggy changed " + piggy.name);
                    onPiggyChanged(piggy);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Piggy piggy = dataSnapshot.getValue(Piggy.class);
                if (piggy != null) {
                    piggy.setKey(dataSnapshot.getKey());
                    onPiggyRemoved(piggy);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Notice.debug(getClass(), "failed to load piggies");
            }
        };
        mPiggyRefListener = piggyRefListener;
        mPiggiesRef.orderByChild("timestamp").addChildEventListener(piggyRefListener);
        */
    }

    @Override
    public void onStop() {
        super.onStop();

        // Remove piggy value event listener
        if (mPiggyRefListener != null) {
            mPiggiesRef.removeEventListener(mPiggyRefListener);
        }
        if (mUserRefListener != null) {
            mUserRef.removeEventListener(mUserRefListener);
        }
    }

    public User getUser() {
        if (mUser == null) {
            getUserFromDb();
        }
        return mUser;
    }

    private void getUserFromDb() {
        mUserRef = getFirebaseDatabase().getReferenceFromUrl(Constants.FIREBASE_USERS_URL+getFirebaseAuthUser().getUid());
        ValueEventListener userRefListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Notice.debug(getClass(), "User onDataChange");
                User user = dataSnapshot.getValue(User.class);
                mUser = user;
                onUserDataChange(user);
                getPiggiesFromDb(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mUserRefListener = userRefListener;
        mUserRef.addValueEventListener(mUserRefListener);
    }

    public void getPiggiesFromDb(User user) {
        for (String piggyKey : user.piggies.keySet()) {
            getFirebaseDatabase().getReference().child("piggies/" + piggyKey).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    Notice.debug(getClass(), "Piggy onDataChange");
                    String piggyKey = snapshot.getKey();
                    Piggy piggy = snapshot.getValue(Piggy.class);
                    if (piggy != null) {
                        piggy.setKey(piggyKey);
                        Notice.debug(getClass(), "Piggy added " + piggy.name + " key: " + piggyKey);
                        onPiggyChanged(IS_FIRST_LOAD, piggy);
                        if (IS_FIRST_LOAD) IS_FIRST_LOAD = false;
                    } else {
                        // Piggy deleted so remove the key from the piggies user list
                        onPiggyRemoved(piggyKey);
                        PiggyInteractor.removeUserPiggy(piggyKey, mAuthUser.getUid());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Notice.debug(getClass(), "Piggy cancelled ");
                }
            });
        }
    }

    public void setCurrentPiggy(Piggy piggy) {
        mCurrentPiggy = piggy;
    }

    public Piggy getCurrentPiggy() {
        if (mCurrentPiggy == null) {
            //TODO
            //mCurrentPiggy = mUser.mainPiggy;
        }
        return mCurrentPiggy;
    }

    public String getCurrentPiggyKey() {
        if (mCurrentPiggy == null) {
            return getFirebaseAuthUser().getUid();
        }
        return mCurrentPiggy.getKey();
    }

    public void onUserDataChange(User user) {

    }

    public void onPiggyChanged(boolean isFirstLoad, Piggy piggy) {}
    public void onPiggyRemoved(String key) {}

}
