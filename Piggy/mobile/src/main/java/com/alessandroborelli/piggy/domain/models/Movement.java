package com.alessandroborelli.piggy.domain.models;

import com.google.firebase.database.Exclude;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alessandroborelli on 03/09/16.
 */

public class Movement {

    public double amount;
    public int type; // Can be INCOME = 1, EXPENSE = 2
    public int category;
    public String description;
    public String creationDate;
    public long timestamp;

    public Movement() {
        // Default constructor required for calls to DataSnapshot.getValue(Movement.class)
    }

    public Movement(double amount, int category, String creationDate, String description, int type) {
        this.amount = amount;
        this.category = category;
        this.creationDate = creationDate;
        this.timestamp = -1 * new Date().getTime();
        this.description = description;
        this.type = type;
    }

    @Override
    public String toString() {
        return "£"+amount + " "+description;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("amount", amount);
        result.put("type", type);
        result.put("category", category);
        result.put("description", description);
        result.put("creationDate", creationDate);
        result.put("timestamp", timestamp);
        return result;
    }
}
