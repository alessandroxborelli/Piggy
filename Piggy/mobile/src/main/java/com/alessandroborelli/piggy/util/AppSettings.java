package com.alessandroborelli.piggy.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by br00 on 15/08/2016.
 */
public class AppSettings {

    public static final String IS_FIRST_TIME = "appSettings_is_first_time";
    public static final String LOGGED = "appSettings_logged";

    private SharedPreferences mSharedPreferences;
    private Context mContext;

    public AppSettings(Context aContex){
        mContext = aContex;
        mSharedPreferences = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
    }

    public boolean getBoolean(String aKey){
        return mSharedPreferences.getBoolean(aKey, false);
    }

    public void setBoolean(String aKey, boolean aValue){
        mSharedPreferences.edit().putBoolean(aKey,aValue).commit();
    }
}
