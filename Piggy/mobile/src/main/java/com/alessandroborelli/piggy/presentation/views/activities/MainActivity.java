package com.alessandroborelli.piggy.presentation.views.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.Movement;
import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.domain.models.User;
import com.alessandroborelli.piggy.presentation.interactors.PiggyInteractor;
import com.alessandroborelli.piggy.presentation.views.custom.NewMovementFView;
import com.alessandroborelli.piggy.presentation.views.custom.NavigationHeaderView;
import com.alessandroborelli.piggy.presentation.views.fragments.MovementsListFragment;
import com.alessandroborelli.piggy.util.BundleKeys;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.Notice;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends PiggyActivity implements NavigationView.OnNavigationItemSelectedListener, NavigationHeaderView.OnPiggySelectedListener, NewMovementFView.OnMovementsFViewListener {

    //private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mMovementsRef;

    //private TextView movementsTV;
    private NewMovementFView movementsFView;
    private FloatingActionButton mFab;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private NavigationHeaderView mNavigationHeaderView;
    private Toolbar mToolbar;

    private MovementsListFragment mMovementsListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //movementsTV = ((TextView)findViewById(R.id.todoTV));

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationHeaderView = (NavigationHeaderView)mNavigationView.inflateHeaderView(R.layout.nav_header_main);
        mNavigationHeaderView.setPiggySelectedListener(this);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //movementsFView.show();
                //mFab.hide();
                mMovementsListFragment.addDummyMovement();
            }
        });

        movementsFView = (NewMovementFView) findViewById(R.id.main_movements_fview);
        movementsFView.setListener(this);
        movementsFView.setAnchorFAB(mFab);

        mMovementsListFragment = new MovementsListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_master_fragment, mMovementsListFragment, MovementsListFragment.TAG).commit();

        mAuth = FirebaseAuth.getInstance();

        boolean isUserSignedIn = getIntent().getExtras().getBoolean(BundleKeys.IS_USER_SIGNED_IN);
        if (isUserSignedIn) {
            if (mAuthUser != null) {
                // User is signed in
                // Name, email address, and profile photo Url
                Notice.debug(getClass(), "onCreate() -> mAuthUser:" + mAuthUser.getUid() + "load some stuff");
                String fullname = mAuthUser.getDisplayName();
                String email = mAuthUser.getEmail();
                Uri photoUrl = mAuthUser.getPhotoUrl();
                Notice.debug(getClass(), "onCreate() -> mAuthUser URL:" + photoUrl);
                //Picasso.with(MainActivity.this).load(photoUrl).into((ImageView) findViewById(R.id.account_image_profile_IV));

                mNavigationHeaderView.setAccountName(fullname);
                mNavigationHeaderView.setAccountImage(MainActivity.this, photoUrl);
                //((TextView) findViewById(R.id.textView2)).setText(name + " " + email + " " + photoUrl);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_log_out) logout();
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_activities_AT) {

        } else if (id == R.id.nav_overview_AT) {

        } else if (id == R.id.nav_settings_AT) {

        }

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else if(movementsFView.isVisible()) {
            movementsFView.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    public void onAddButtonClick(View v){
        String currentDateTimeString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date());
        Movement movement = new Movement((float) Math.random()*1000, 2, currentDateTimeString, "description", 2);
        mMovementsRef.push().setValue(movement);
        Log.d(Constants.TAG, "on add click");
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuthUser == null) mAuth.addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(this);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        mAuthUser = firebaseAuth.getCurrentUser();
        if (mAuthUser != null) {
            // User is signed in
            Notice.debug(getClass(), " -> onAuthStateChanged() -> signed in:" + mAuthUser.getUid());
            // User is signed in
            // Name, email address, and profile photo Url
            String name = mAuthUser.getDisplayName();
            String email = mAuthUser.getEmail();
            Uri photoUrl = mAuthUser.getPhotoUrl();
            Picasso.with(MainActivity.this).load(photoUrl).into((ImageView) findViewById(R.id.account_image_profile_IV));

            ((TextView) findViewById(R.id.textView2)).setText(name + " " + email + " " + photoUrl);

        } else {
            // User is signed out
            Notice.debug(getClass(), " -> onAuthStateChanged() -> signed_out!");
            Intent intent = new Intent(this, AuthenticationActivity.class);
            startActivity(intent);
            mAuth.removeAuthStateListener(this);
            finish();
        }
    }

    @Override
    public void onUserDataChange(User user) {
        super.onUserDataChange(user);
        if (user != null) {
            Notice.debug(getClass(), "onUserDataChange() -> User: "+user.email);
            // Update navigation view with the user info
            //mNavigationHeaderView.clearPiggies();
            //mNavigationHeaderView.addPiggy(user.mainPiggy);
        }
    }

    @Override
    public void onPiggyChanged(boolean isFirstLoad, Piggy piggy) {
        super.onPiggyChanged(isFirstLoad, piggy);
        mNavigationHeaderView.addPiggy(piggy);
        if(mMovementsListFragment != null && piggy.isMainPiggy()) {
            setCurrentPiggy(piggy);
            mNavigationHeaderView.updatePiggyHeader();
            if (isFirstLoad) {
                // refresh movements fragment
                mMovementsListFragment = new MovementsListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.main_master_fragment, mMovementsListFragment, MovementsListFragment.TAG).commit();
            }
        }
    }

    @Override
    public void onPiggyRemoved(String key) {
        super.onPiggyRemoved(key);
        mNavigationHeaderView.removePiggy(key);
    }

    /**
     * NavigationHeaderView.OnPiggySelectedListener
     * @param piggy The selected account
     *
     * @return
     */

    @Override
    public boolean onPiggySelected(Piggy piggy) {
        setCurrentPiggy(piggy);
        // refresh movements fragment
        mMovementsListFragment = new MovementsListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_master_fragment, mMovementsListFragment, MovementsListFragment.TAG).commit();
        Toast.makeText(MainActivity.this, "piggy selected "+piggy.getName(), Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onEditPiggySelected(View view, final Piggy piggy) {
        PopupMenu popupMenu = new PopupMenu(MainActivity.this, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mNavigationHeaderView.showMainList();
                if(item.getItemId() == R.id.popu_piggy_edit_AT){
                    Toast.makeText(MainActivity.this, "edit piggy", Toast.LENGTH_SHORT).show();
                } else if(item.getItemId() == R.id.popu_piggy_remove_AT){
                    mNavigationHeaderView.removePiggy(piggy.getKey());
                    Toast.makeText(MainActivity.this, "remove piggy", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        popupMenu.inflate(R.menu.popup_piggy);
        popupMenu.show();
    }

    @Override
    public void onAddNewPiggy() {
        Toast.makeText(MainActivity.this, "add piggy", Toast.LENGTH_SHORT).show();
        int randNum = (int)((Math.random()+1)*100);
        Piggy piggy = new Piggy().setIconResource(R.drawable.ic_dashboard).setName("name "+randNum).setIncome(randNum*2);
        PiggyInteractor.add(piggy, mAuthUser.getUid());
    }


    /**
     * NewMovementFView.OnMovementsFViewListener
     */

    @Override
    public void onCategoryClick() {

    }

    @Override
    public void onAddClick() {

    }

    @Override
    public void onShow() {

    }

    @Override
    public void onDismiss() {

    }
}
