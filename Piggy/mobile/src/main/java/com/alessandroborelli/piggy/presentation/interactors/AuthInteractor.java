package com.alessandroborelli.piggy.presentation.interactors;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.domain.models.User;
import com.alessandroborelli.piggy.presentation.presenters.IAuthVPI;
import com.alessandroborelli.piggy.util.AppSettings;
import com.alessandroborelli.piggy.util.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by br00 on 25/05/2016.
 */
public class AuthInteractor implements IAuthVPI.InteractorActions{

    private FirebaseAuth mAuth;
    private Activity mActivity;
    private IAuthVPI.RequiredPresenterActions mPresenter;
    private AppSettings mAppSettings;
    private Stage mStage;

    public AuthInteractor(IAuthVPI.RequiredPresenterActions aPresenter, Context aContext){
        mPresenter = aPresenter;
        mAuth = FirebaseAuth.getInstance();
        mActivity = (Activity) aContext;
        mAppSettings = new AppSettings(mActivity);
        mStage = Stage.LOGGED_OUT;
    }


    /**
     * PRESENTER  ->  INTERACTOR
     */

    @Override
    public void authenticate(String typedEmail, String typedPassword) {

        if (!validateEmail(typedEmail)) {
            return;
        }

        if (!validatePassword(typedPassword)) {
            return;
        }

        signIn(typedEmail, typedPassword);
    }

    @Override
    public void enrollNewUser(String firstName, String lastName, String typedEmail, String typedPassword) {

        if (!validateFirstName(typedEmail)) {
            return;
        }

        if (!validateLastName(typedEmail)) {
            return;
        }

        if (!validateEmail(typedEmail)) {
            return;
        }

        if (!validatePassword(typedPassword)) {
            return;
        }

        signUp(firstName, lastName, typedEmail, typedPassword);
    }

    @Override
    public void resetPassword() {

    }

    @Override
    public void logout() {
        mAppSettings.setBoolean(AppSettings.LOGGED, false);
        mStage = Stage.LOGGED_OUT;
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public void checkStage() {
        if (mAppSettings.getBoolean(AppSettings.LOGGED)) {
            mStage = Stage.LOGGED_IN;
        } else {
            mStage = Stage.LOGGED_OUT;
        }
        mPresenter.onUpdate(mStage);
    }

    @Override
    public void onDestroy() {

    }

    /**
     * END
     */

    /**
     * Form utils
     */

    private boolean validateFirstName(String aName) {
        if (!isValidFirstName(aName)) {
            mPresenter.onError(Constants.ERROR_AUTH_FIRST_NAME_NOT_VALID, Constants.EMPTY_STRING);
            return false;
        } else {
            mPresenter.onUpdate(Stage.FIRST_NAME_VALID);
        }
        return true;
    }

    private boolean validateLastName(String aName) {
        if (!isValidLastName(aName)) {
            mPresenter.onError(Constants.ERROR_AUTH_LAST_NAME_NOT_VALID, Constants.EMPTY_STRING);
            return false;
        } else {
            mPresenter.onUpdate(Stage.LAST_NAME_VALID);
        }
        return true;
    }

    private boolean validateEmail(String anEmail) {
        if (!isValidEmail(anEmail)) {
            mPresenter.onError(Constants.ERROR_AUTH_EMAIL_NOT_VALID, Constants.EMPTY_STRING);
            return false;
        } else {
            mPresenter.onUpdate(Stage.EMAIL_VALID);
        }
        return true;
    }

    private boolean validatePassword(String aPassword) {
        if (!isValidPassword(aPassword)) {
            mPresenter.onError(Constants.ERROR_AUTH_PASSWORD_NOT_VALID, Constants.EMPTY_STRING);
            return false;
        } else {
            mPresenter.onUpdate(Stage.PASSWORD_VALID);
        }

        return true;
    }

    public static boolean isValidFirstName(String name) {
        return !TextUtils.isEmpty(name) && name.length() > 1;
    }

    public static boolean isValidLastName(String name) {
        return !TextUtils.isEmpty(name) && name.length() > 1;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return !password.trim().isEmpty();
    }


    public void signIn(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(Constants.TAG, "signInWithEmail", task.getException());
                            Toast.makeText(mActivity, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        } else {
                            mAppSettings.setBoolean(AppSettings.LOGGED, true);
                            mPresenter.onAuthenticated();
                        }
                    }
                });
    }

    public void signUp(final String firstName, final String lastName, String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(Constants.TAG, "createUserWithEmail", task.getException());
                            Toast.makeText(mActivity, "User creation failed. "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        } else {

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(firstName+" "+lastName)
                                    .build();
                            FirebaseUser user = mAuth.getCurrentUser();
                            //TODO send verification email https://firebase.google.com/docs/auth/android/manage-users
                            // Write the new user in the database
                            String currentDateTimeString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date());
                            writeNewUserIntoDatabase(user.getUid(), firstName+" "+lastName, user.getEmail(), currentDateTimeString);
                            // Update the user info in the auth firebase
                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(Constants.TAG, "User profile updated.");
                                                mAppSettings.setBoolean(AppSettings.LOGGED, true);
                                                mPresenter.onAuthenticated();
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void writeNewUserIntoDatabase(String uid, String fullname, String email, String creationDate) {
        // Create main Piggy
        Piggy mainPiggy = new Piggy().setName(mActivity.getString(R.string.my_piggy)).setCreatedBy(uid).setType(Piggy.TYPE.MAIN);
        String piggyKey = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_PIGGIES_URL).push().getKey();
        Map<String, Object> piggyValues = mainPiggy.toMap();

        // Create User
        User user = new User(fullname, email, creationDate, piggyKey);
        user.addPiggy(piggyKey);
        Map<String, Object> userValues = user.toMap();

        // Write into database
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/piggies/" + piggyKey, piggyValues);
        childUpdates.put("/users/" + uid, userValues);
        FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);
    }

    /**
     * Enumeration to indicate the status of the session.
     */

    public enum Stage {
        FIRST_NAME_VALID,
        LAST_NAME_VALID,
        EMAIL_VALID,
        PASSWORD_VALID,
        LOGGED_IN,
        LOGGED_OUT
    }
}
