package com.alessandroborelli.piggy.presentation.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.Movement;
import com.alessandroborelli.piggy.presentation.views.custom.ItemTouchHelperListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aborelli on 05/01/16.
 */
public class MovementAdapter extends SelectableAdapter<MovementAdapter.ViewHolder> implements ItemTouchHelperListener {

    // Define mListener member variable
    private static OnItemListener mListener;

    // Define the mListener interface
    public interface OnItemListener {
        void onItemClick(View itemView, int pos);
        boolean onItemLongClick(View itemView, int pos);
        void onItemSwiped(Movement movement, String key);
    }
    // Define the method that allows the parent activity or fragment to define the mListener
    public void setOnItemClickListener(OnItemListener listener) {
        mListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{

        public TextView amount, description, date;
        public LinearLayout foregroundView;
        public LinearLayout backgroundView;

        public ViewHolder(View itemView) {
            super(itemView);

            amount = (TextView)itemView.findViewById(R.id.adapter_movement_amount_TV);
            description = (TextView)itemView.findViewById(R.id.adapter_movement_description_TV);
            date = (TextView)itemView.findViewById(R.id.adapter_movement_date_TV);
            foregroundView = (LinearLayout)itemView.findViewById(R.id.mainAdapterForeground);
            backgroundView = (LinearLayout)itemView.findViewById(R.id.mainAdapterBackground);
            if(foregroundView != null) {
                foregroundView.setOnClickListener(this);
                foregroundView.setOnLongClickListener(this);
            }
        }

        public View getSwipeableView() {
            return foregroundView;
        }

        public View getActionsView(){
            return backgroundView;
        }

        @Override
        public void onClick(View view) {
            Log.d("db", "click " + getLayoutPosition());
            if (mListener != null)
                mListener.onItemClick(itemView, getLayoutPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            Log.d("db", "long click " + getLayoutPosition());
            if (mListener != null)
                return mListener.onItemLongClick(itemView, getLayoutPosition());
            return false;
        }

    }

    public static class ProgressViewHolder extends ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBarItem);
        }
    }


    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_FOOTER = 1;

    private List<Movement> mMovementsList;
    private List<String> mKeys;

    private Context context;

    public MovementAdapter(){
        mMovementsList = new ArrayList<>();
        mKeys = new ArrayList<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_BASIC) {
            return onCreateBasicItemViewHolder(parent, viewType);
        } else if (viewType == ITEM_VIEW_TYPE_FOOTER) {
            return onCreateFooterViewHolder(parent, viewType);
        } else {
            throw new IllegalStateException("Invalid type, this type ot items " + viewType + " can't be handled");
        }

    }

    public ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        //noinspection ConstantConditions
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_movement_item, parent, false);
        return new ViewHolder(v);
    }

    public ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        //noinspection ConstantConditions
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progressbar_item, parent, false);
        return new ProgressViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE_BASIC) {
            onBindBasicItemView(holder, position);
        } else {
            onBindFooterView(holder, position);
        }
    }

    public void onBindBasicItemView(ViewHolder holder, int position) {
        if(holder != null) {
            Movement item = mMovementsList.get(position);
            holder.amount.setText("£"+item.amount);
            holder.description.setText(item.description);
            holder.date.setText(item.creationDate);
            int defBackgroundColor = this.context.getResources().getColor(R.color.recyclerview_item_def);
            int selectedBackgroundColor = this.context.getResources().getColor(R.color.recyclerview_item_checked);

            //swipe
            //holder.getSwipeableView().setTranslationX(isSwiped(position) ? getSwipedItemDirection()*holder.getSwipeableView().getWidth() : 0);

            //highlight
            holder.getSwipeableView().setBackgroundColor(isSelected(position) ? selectedBackgroundColor : defBackgroundColor);
        }
    }

    public void onBindFooterView(MovementAdapter.ViewHolder genericHolder, int position) {
        ((ProgressViewHolder)genericHolder).progressBar.setIndeterminate(true);
    }

    @Override
    public int getItemCount() {
        return mMovementsList.size();
    }//getItemCount

    @Override
    public int getItemViewType(int position) {
        return mMovementsList.get(position) != null ? ITEM_VIEW_TYPE_BASIC : ITEM_VIEW_TYPE_FOOTER;
    }

    public Movement get(int pos){ return mMovementsList.get(pos); }

    public void add(Movement movement, String key) {
        mMovementsList.add(movement);
        mKeys.add(key);
        notifyItemInserted(mMovementsList.size());
    }

    public void addAtFirst(Movement movement, String key) {
        mMovementsList.add(0, movement);
        mKeys.add(0, key);
        notifyItemInserted(0);
    }

    public void update(Movement movement, String key) {
        int index = indexOf(key);
        mMovementsList.set(index, movement);
        notifyItemChanged(index);
    }

    public void remove(String key) {
        int index = indexOf(key);
        mKeys.remove(index);
        mMovementsList.remove(index);
        notifyItemRemoved(index);
    }

    public int indexOf(String key) {
        return mKeys.indexOf(key);
    }


    /**
     * ItemTouchHelperListener
     * @param pos
     */
    @Override
    public void onItemDismiss(int pos) {
        // Find the key of the item by position of the swiped item
        String key = mKeys.get(pos);
        mListener.onItemSwiped(mMovementsList.get(pos), key);
    }

}
