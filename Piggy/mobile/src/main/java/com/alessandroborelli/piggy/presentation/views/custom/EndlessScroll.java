package com.alessandroborelli.piggy.presentation.views.custom;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by aborelli on 30/11/15.
 */
public abstract class EndlessScroll extends RecyclerView.OnScrollListener {

    private OnEndlessScrollListener listener;

    public interface OnEndlessScrollListener {
        void onLoadMore(int currentPage);
        void onScrollStateChanged(int newState);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnEndlessScrollListener(OnEndlessScrollListener listener) {
        this.listener = listener;
    }

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.
    private int totalItemForPage = 25;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessScroll(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }//EndlessScroll

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal && totalItemCount%totalItemForPage==0) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }

        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached

            // Do something
            current_page++;

            if (listener != null) listener.onLoadMore(current_page);

            loading = true;
        }
    }//onScrolled

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        if (listener != null) listener.onScrollStateChanged(newState);
    }

}//class
