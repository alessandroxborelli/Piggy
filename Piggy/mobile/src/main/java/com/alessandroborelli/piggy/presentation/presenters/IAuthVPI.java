package com.alessandroborelli.piggy.presentation.presenters;

import com.alessandroborelli.piggy.presentation.interactors.AuthInteractor;

/**
 * Created by br00 on 07/06/2016.
 *
 * Groups all connection operations between VPI pattern layer:
 * View, Presenter and Interactor.
 * The idea is that you can understand all worflow only reading this class.
 *
 * Basically the code follow this worflow:
 *
 * 1. User click on the login button
 * 2. [View] -> [Presenter] -> [Interactor]
 * 3. [Interactor] -> [Presenter] -> [View]
 * 4. End of scenario
 *
 * See the decription on github to have more idea on how does it work VPI.
 *
 */

public interface IAuthVPI {

    /**
     * Presenter operations available from the View
     *      View -> Presenter
     */
    interface PresenterActions{
        void onCreate();
        void onResume();
        void onStop();
        void onDestroy();
        void requestAuthentication(String typedEmail, String typedPassword);
        void requestEnrollNewUser(String firstName, String lastName, String typedEmail, String typedPassword);
        void requestLogout();
    }

    /**
     * Interactor operations available from the Presenter
     *      Presenter -> Interactor
     */
    interface InteractorActions {
        void authenticate(String typedEmail, String typedPassword);
        void enrollNewUser(String firstName, String lastName, String typedEmail, String typedPassword);
        void resetPassword();
        void logout();
        void checkStage();
        void onDestroy();
    }

    /**
     * Presenter operations available from the Interactor
     *      Interactor -> Presenter
     */
    interface RequiredPresenterActions {
        void onAuthenticated();
        void onPasswordResetted();
        void onUpdate(AuthInteractor.Stage aStage);
        void onError(int errorId, String errorMsg);
    }

    /**
     * View operations available from the Presenter
     *      Presenter -> View
     */
    interface RequiredViewActions {
        void goToHome();
        void updateUI(AuthInteractor.Stage aStage);
        void showError(int errorId, String msg);
    }

}
