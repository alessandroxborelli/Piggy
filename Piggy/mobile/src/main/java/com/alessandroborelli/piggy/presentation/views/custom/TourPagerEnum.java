package com.alessandroborelli.piggy.presentation.views.custom;

import com.alessandroborelli.piggy.R;

/**
 * Created by alessandroborelli on 17/08/16.
 */

public enum TourPagerEnum {

    // TODO change colors, descriptions and images

    // Background color, Description and Image
    HOME(R.color.tour_home, R.string.tour_home_description, R.mipmap.ic_launcher),
    FIRST(R.color.tour_first, R.string.tour_first_description, R.mipmap.ic_launcher),
    SECOND(R.color.tour_second, R.string.tour_second_description, R.mipmap.ic_launcher),
    THIRD(R.color.tour_third, R.string.tour_third_description, R.mipmap.ic_launcher);

    private int mBackgroundColorResId;
    private int mDescriptionResId;
    private int mImageResId;

    TourPagerEnum(int aBackgroundColorResId, int aDescriptionResId, int aImageResId) {
        mBackgroundColorResId = aBackgroundColorResId;
        mDescriptionResId = aDescriptionResId;
        mImageResId = aImageResId;
    }

    public int getBackgroundColorResId() {
        return mBackgroundColorResId;
    }

    public int getDescriptionResId() {
        return mDescriptionResId;
    }

    public int getImageResId() {
        return mImageResId;
    }
}
