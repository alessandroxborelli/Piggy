package com.alessandroborelli.piggy.presentation.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.presentation.views.custom.TourPagerAdapter;
import com.alessandroborelli.piggy.presentation.views.fragments.JoinFragment;
import com.alessandroborelli.piggy.presentation.views.fragments.SignInFragment;
import com.alessandroborelli.piggy.util.BundleKeys;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.Notice;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

/**
 * Created by alessandroborelli on 13/07/16.
 */

public class AuthenticationActivity extends AppCompatActivity {

    private ViewPager mTourViewPager;
    private FrameLayout mFragmentContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        mFragmentContainer = (FrameLayout) findViewById(R.id.auth_container_FRAG);
        mTourViewPager = (ViewPager)findViewById(R.id.auth_tour_VP);
        mTourViewPager.setAdapter(new TourPagerAdapter(this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == Constants.REQUEST_CODE_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Notice.debug(getClass(), "Google handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount account = result.getSignInAccount();
            firebaseAuthWithGoogle(account);
        } else {
            // Signed out, show unauthenticated UI.
            Notice.error(getClass(), "Google error auth");
            Notice.toast(AuthenticationActivity.this, "Google Authentication failed.");
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Notice.debug(getClass(), "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Notice.debug(getClass(), "firebaseAuthWithGoogle() -> signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(Constants.TAG, "signInWithCredential", task.getException());
                            Notice.toast(AuthenticationActivity.this, "Google Authentication failed.");
                        } else {
                            // Go to MainActivity
                            Notice.toast(AuthenticationActivity.this, "auth google "+acct.getDisplayName());
                            Intent intent = new Intent(AuthenticationActivity.this, MainActivity.class);
                            intent.putExtra(BundleKeys.IS_USER_SIGNED_IN, true);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
    }

    public void onJoinClick(View view) {
        showJoinUs(false);
    }

    public void onSignInClick(View view) {
        showSignIn(false);
    }

    public void showSignIn(boolean swap) {
        mFragmentContainer.setVisibility(View.VISIBLE);
        if (swap) {
            getSupportFragmentManager().popBackStack();
        }
        SignInFragment fragment = new SignInFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_container_FRAG, fragment, SignInFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    public void showJoinUs(boolean swap) {
        mFragmentContainer.setVisibility(View.VISIBLE);
        if (swap) {
            getSupportFragmentManager().popBackStack();
        }
        JoinFragment fragment = new JoinFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.auth_container_FRAG, fragment, JoinFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            mFragmentContainer.setVisibility(View.GONE);
        }
    }
}
