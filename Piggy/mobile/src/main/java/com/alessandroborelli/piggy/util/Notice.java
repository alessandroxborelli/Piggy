package com.alessandroborelli.piggy.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by alessandroborelli on 28/08/16.
 */

public class Notice {

    public static void debug(Class aClass, String mes) {
        Log.d(Constants.TAG, aClass.getSimpleName()+ " -> "+mes);
    }

    public static void error(Class aClass, String mes) {
        Log.e(Constants.TAG, aClass.getSimpleName()+ " -> "+mes);
    }

    public static void toast(Context context, String mes) {
        Toast.makeText(context, mes, Toast.LENGTH_SHORT).show();
    }

}
