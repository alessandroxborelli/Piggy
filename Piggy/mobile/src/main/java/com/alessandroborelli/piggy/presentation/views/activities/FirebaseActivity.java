package com.alessandroborelli.piggy.presentation.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alessandroborelli.piggy.util.Notice;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by alessandroborelli on 28/08/16.
 */

public class FirebaseActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener{

    protected FirebaseAuth mAuth;
    protected FirebaseUser mAuthUser;
    protected FirebaseDatabase mDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Authentication object
        mAuth = FirebaseAuth.getInstance();

        // Current auth user
        mAuthUser = mAuth.getCurrentUser();

        // Firebase database
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuthUser == null) mAuth.addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(this);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        mAuthUser = firebaseAuth.getCurrentUser();
        if (mAuthUser != null) {
            // User is signed in
            Notice.debug(getClass(), "onAuthStateChanged() -> signed in:" + mAuthUser.getUid());
        } else {
            // User is signed out
            Notice.debug(getClass(), "onAuthStateChanged() -> signed_out!");
            Intent intent = new Intent(this, AuthenticationActivity.class);
            startActivity(intent);
            mAuth.removeAuthStateListener(this);
            finish();
        }
    }

    protected void logout() {
        Notice.debug(getClass(), "logout!");
        mAuthUser = null;
        mAuth.addAuthStateListener(this);
        FirebaseAuth.getInstance().signOut();
    }

    public FirebaseUser getFirebaseAuthUser(){
        if (mAuthUser == null) {
            mAuthUser = mAuth.getCurrentUser();
        }
        return mAuthUser;
    }

    public FirebaseDatabase getFirebaseDatabase(){
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        return mDatabase;
    }
}
