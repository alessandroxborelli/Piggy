package com.alessandroborelli.piggy.domain.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alessandroborelli on 03/09/16.
 */

public class User {

    public String fullname;
    public String email;
    public String creationDate; // format 23 Aug 2016
    public String mainPiggy;
    public Map<String, Object> piggies = new HashMap<>();

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String fullname, String email, String creationDate, String mainPiggy) {
        this.fullname = fullname;
        this.email = email;
        this.creationDate = creationDate;
        this.mainPiggy = mainPiggy;
    }

    public void addPiggy(String piggyKey) {
        piggies.put(piggyKey, true);
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fullname", fullname);
        result.put("email", email);
        result.put("creationDate", creationDate);
        result.put("mainPiggy", mainPiggy);
        result.put("piggies", piggies);
        return result;
    }

}