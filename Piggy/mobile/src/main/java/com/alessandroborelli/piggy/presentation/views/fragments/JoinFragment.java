package com.alessandroborelli.piggy.presentation.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.presentation.interactors.AuthInteractor;
import com.alessandroborelli.piggy.presentation.presenters.AuthPresenter;
import com.alessandroborelli.piggy.presentation.presenters.IAuthVPI;
import com.alessandroborelli.piggy.presentation.views.activities.AuthenticationActivity;
import com.alessandroborelli.piggy.presentation.views.activities.MainActivity;
import com.alessandroborelli.piggy.util.BundleKeys;
import com.alessandroborelli.piggy.util.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by alessandroborelli on 17/08/16.
 */

public class JoinFragment extends Fragment implements View.OnClickListener, IAuthVPI.RequiredViewActions {

    public static final String TAG = "JoinFragment";

    private IAuthVPI.PresenterActions mPresenter;

    private TextInputLayout mFirstNameInputLayout;
    private TextInputLayout mLastNameInputLayout;
    private TextInputLayout mEmailInputLayout;
    private TextInputLayout mPasswordInputLayout;
    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private Button mSignInButton;
    private Button mJoinUsButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setRetainInstance(true);
        if (savedInstanceState == null) {
            // Creates presenter
            mPresenter = new AuthPresenter(this, getActivity());
            mPresenter.onCreate();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_auth_join, container, false);
        mFirstNameInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_join_layout_first_name);
        mLastNameInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_join_layout_last_name);
        mEmailInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_join_layout_email);
        mPasswordInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_join_layout_password);
        mFirstNameEditText = (EditText) rootView.findViewById(R.id.auth_join_first_name_ET);
        mLastNameEditText = (EditText) rootView.findViewById(R.id.auth_join_last_name_ET);
        mEmailEditText = (EditText) rootView.findViewById(R.id.auth_join_email_ET);
        mPasswordEditText = (EditText) rootView.findViewById(R.id.auth_join_password_ET);
        mSignInButton = (Button) rootView.findViewById(R.id.auth_join_signIn_BT);
        mJoinUsButton = (Button) rootView.findViewById(R.id.auth_join_submit_BT);
        mSignInButton.setOnClickListener(this);
        mJoinUsButton.setOnClickListener(this);
        mFirstNameEditText.addTextChangedListener(new AuthFormTextWatcher(mFirstNameEditText));
        mLastNameEditText.addTextChangedListener(new AuthFormTextWatcher(mLastNameEditText));
        mEmailEditText.addTextChangedListener(new AuthFormTextWatcher(mEmailEditText));
        mPasswordEditText.addTextChangedListener(new AuthFormTextWatcher(mPasswordEditText));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    public void goBackToSignIn() {
        ((AuthenticationActivity)getActivity()).showSignIn(true);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == mSignInButton.getId()) {
            // Back to SignIn fragment
            goBackToSignIn();
        } else if (id == mJoinUsButton.getId()) {
            // Tries to enroll new user
            String typedFirstName = mFirstNameEditText.getText().toString().trim();
            String typedLastName = mLastNameEditText.getText().toString().trim();
            String typedEmail = mEmailEditText.getText().toString().trim();
            String typedPassword = mPasswordEditText.getText().toString();
            mPresenter.requestEnrollNewUser(typedFirstName, typedLastName, typedEmail, typedPassword);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    /**
     * PRESENTER  ->  VIEW
     */

    @Override
    public void goToHome() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra(BundleKeys.IS_USER_SIGNED_IN, true);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void updateUI(AuthInteractor.Stage aStage) {
        switch (aStage) {
            case FIRST_NAME_VALID:
                mFirstNameInputLayout.setError(null);
                mFirstNameInputLayout.setErrorEnabled(false);
                break;
            case LAST_NAME_VALID:
                mLastNameInputLayout.setError(null);
                mLastNameInputLayout.setErrorEnabled(false);
                break;
            case EMAIL_VALID:
                mEmailInputLayout.setError(null);
                mEmailInputLayout.setErrorEnabled(false);
                break;
            case PASSWORD_VALID:
                mPasswordInputLayout.setError(null);
                mPasswordInputLayout.setErrorEnabled(false);
                break;
        }
    }

    @Override
    public void showError(int errorId, String msg) {
        switch (errorId) {
            case Constants.ERROR_AUTH_FIRST_NAME_NOT_VALID:
                mFirstNameInputLayout.setErrorEnabled(true);
                mFirstNameInputLayout.setError(getString(R.string.error_enter_first_name));
                requestFocus(mFirstNameEditText);
                break;
            case Constants.ERROR_AUTH_LAST_NAME_NOT_VALID:
                mLastNameInputLayout.setErrorEnabled(true);
                mLastNameInputLayout.setError(getString(R.string.error_enter_last_name));
                requestFocus(mLastNameEditText);
                break;
            case Constants.ERROR_AUTH_EMAIL_NOT_VALID:
                mEmailInputLayout.setErrorEnabled(true);
                mEmailInputLayout.setError(getString(R.string.error_enter_email));
                requestFocus(mEmailEditText);
                break;
            case Constants.ERROR_AUTH_PASSWORD_NOT_VALID:
                mPasswordInputLayout.setErrorEnabled(true);
                mPasswordInputLayout.setError(getString(R.string.error_password_to_short));
                requestFocus(mPasswordEditText);
                break;
        }
    }

    /**
     * END
     */

    private class AuthFormTextWatcher implements TextWatcher {

        private View view;

        private AuthFormTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.auth_join_first_name_ET:
                    if (AuthInteractor.isValidFirstName(editable.toString())) {
                        Log.d(Constants.TAG,"first name VALID");
                        mFirstNameInputLayout.setError(null);
                        mFirstNameInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"first name NOT VALID");
                        mFirstNameInputLayout.setErrorEnabled(true);
                        mFirstNameInputLayout.setError(getString(R.string.error_enter_first_name));
                        requestFocus(mFirstNameEditText);
                    }
                    break;
                case R.id.auth_join_last_name_ET:
                    if (AuthInteractor.isValidLastName(editable.toString())) {
                        Log.d(Constants.TAG,"last name VALID");
                        mLastNameInputLayout.setError(null);
                        mLastNameInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"last name NOT VALID");
                        mLastNameInputLayout.setErrorEnabled(true);
                        mLastNameInputLayout.setError(getString(R.string.error_enter_last_name));
                        requestFocus(mLastNameEditText);
                    }
                    break;
                case R.id.auth_join_email_ET:
                    if (AuthInteractor.isValidEmail(editable.toString())) {
                        Log.d(Constants.TAG,"email VALID");
                        mEmailInputLayout.setError(null);
                        mEmailInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"email NOT VALID");
                        mEmailInputLayout.setErrorEnabled(true);
                        mEmailInputLayout.setError(getString(R.string.error_enter_email));
                        requestFocus(mEmailEditText);
                    }
                    break;
                case R.id.auth_join_password_ET:
                    if (AuthInteractor.isValidPassword(editable.toString())) {
                        Log.d(Constants.TAG,"password VALID");
                        mPasswordInputLayout.setError(null);
                        mPasswordInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"password NOT VALID");
                        mPasswordInputLayout.setErrorEnabled(true);
                        mPasswordInputLayout.setError(getString(R.string.error_enter_password));
                        requestFocus(mPasswordEditText);
                    }
                    break;
            }
        }
    }
}
