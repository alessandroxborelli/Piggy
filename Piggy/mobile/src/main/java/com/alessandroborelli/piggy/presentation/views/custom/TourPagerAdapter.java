package com.alessandroborelli.piggy.presentation.views.custom;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;

/**
 * Created by alessandroborelli on 17/08/16.
 */

public class TourPagerAdapter extends PagerAdapter {

    private Context mContext;

    public TourPagerAdapter(Context aContext) {
        mContext = aContext;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TourPagerEnum tourPagerEnum = TourPagerEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.pager_tour, container, false);
        layout.setBackgroundColor(mContext.getResources().getColor(tourPagerEnum.getBackgroundColorResId()));
        ((TextView)layout.findViewById(R.id.tour_description_TV)).setText(tourPagerEnum.getDescriptionResId());
        ((ImageView)layout.findViewById(R.id.tour_image_IV)).setImageResource(tourPagerEnum.getImageResId());
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View)view);
    }

    @Override
    public int getCount() {
        return TourPagerEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
