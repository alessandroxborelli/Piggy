package com.alessandroborelli.piggy.presentation.presenters;

import android.content.Context;

import com.alessandroborelli.piggy.presentation.interactors.AuthInteractor;

/**
 * Created by br00 on 25/05/2016.
 */
public class AuthPresenter implements IAuthVPI.PresenterActions, IAuthVPI.RequiredPresenterActions{

    private IAuthVPI.RequiredViewActions mView;
    private IAuthVPI.InteractorActions mInteractors;

    public AuthPresenter(IAuthVPI.RequiredViewActions aView, Context aContext){
        mView = aView;
        mInteractors = new AuthInteractor(this, aContext);
    }


    /**
     * VIEW  ->  PRESENTER
     */

    @Override
    public void onCreate() {

    }

    @Override
    public void onResume() {
        mInteractors.checkStage();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void requestAuthentication(String typedEmail, String typedPassword) {
        mInteractors.authenticate(typedEmail, typedPassword);
    }

    @Override
    public void requestEnrollNewUser(String firstName, String lastName, String typedEmail, String typedPassword) {
        mInteractors.enrollNewUser(firstName, lastName, typedEmail, typedPassword);
    }

    @Override
    public void requestLogout() {
        mInteractors.logout();
    }

    /**
     * END
     */


    /**
     * INTERACTOR  ->  PRESENTER
     */

    @Override
    public void onAuthenticated() {
        mView.goToHome();
    }

    @Override
    public void onPasswordResetted() {

    }

    @Override
    public void onUpdate(AuthInteractor.Stage aStage) {
        mView.updateUI(aStage);
    }

    @Override
    public void onError(int errorId, String errorMsg) {
        mView.showError(errorId, errorMsg);
    }

    /**
     * END
     */
}
