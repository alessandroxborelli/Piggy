package com.alessandroborelli.piggy.presentation.views.adapters;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aborelli on 05/01/16.
 */
public abstract class SelectableAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private static final String SINGLE_SELECTION_POSITION = "singlePosition";
    private static final String MULTI_SELECTION_POSITIONS = "multiPosition";
    private static final String SELECTIONS_STATE = "state";

    public static final int SINGLE_SWIPE_MODE = 3;
    public static final int SINGLE_SELECTION_MODE = 1;
    public static final int MULTI_SELECTION_MODE = 2;

    public static final int SWIPE_DIR_NONE = 0;

    private int state;

    //swiped item is a pair of position and exit direction
    private Pair<Integer,Integer> swipedItem;
    private int selectedItem;
    private SparseBooleanArray selectedItems;

    public SelectableAdapter() {
        setState(SINGLE_SELECTION_MODE);
        swipedItem = Pair.create(-1,SWIPE_DIR_NONE);
        selectedItem = -1;
        selectedItems = new SparseBooleanArray();
    }//constructor

    public void setState(int state){
        if(state == SINGLE_SWIPE_MODE && getSwipedItem() != -1) restoreSwipe();
        else if(state == SINGLE_SELECTION_MODE && selectedItem != -1) restoreSingleSelection();
        else if(state == MULTI_SELECTION_MODE) {clearSingleSelection();clearSwipe();}
        this.state = state;
    }//setState

    public int getState(){
        return this.state;
    }//getState

    /**
     * Indicates if the item at position position is selected
     */
    public boolean isSelected(int position) {
        if (getState() == SINGLE_SELECTION_MODE || getState() == SINGLE_SWIPE_MODE) {
            return getSelectedItem()==position;
        } else if (getState() == MULTI_SELECTION_MODE) {
            return getSelectedItems().contains(position);
        }
        return false;
    }//isSelected

    public boolean isSwiped(int position) {
        if (getState() == SINGLE_SWIPE_MODE) {
            return getSwipedItem() == position;
        }
        return false;
    }//isSwiped

    public void setSwipedItem(int position, int direction){
        if(getSwipedItem() != position){
            notifyItemChanged(getSwipedItem());
        }
        swipedItem = Pair.create(position,direction);
        notifyItemChanged(getSwipedItem());
    }//setSwipedItem

    public void singleSelection(int position){
        if(selectedItem != position){
            notifyItemChanged(selectedItem);
        }
        selectedItem = position;
        notifyItemChanged(selectedItem);
    }//singleSelection

    public void toggleSelection(int position) {
        if (selectedItems.get(position, false)) {
            selectedItems.delete(position);
        } else {
            selectedItems.put(position, true);
        }
        notifyItemChanged(position);
    }//toggleSelection

    public void restoreSwipe(){
        notifyItemChanged(getSwipedItem());
    }//restoreSwipe

    public void restoreSingleSelection(){
        notifyItemChanged(selectedItem);
    }//restoreSingleSelection

    public void clearSingleSelection(){
        int tempSelectedItem = selectedItem;
        selectedItem = -1;
        notifyItemChanged(tempSelectedItem);
        selectedItem = tempSelectedItem;
    }//clearSingleSelection

    public void clearSwipe(){
        int tempSwipedItem = getSwipedItem();
        swipedItem = Pair.create(-1,SWIPE_DIR_NONE);
        notifyItemChanged(tempSwipedItem);
    }//clearSwipe

    /**
     * Clear the selection status for all items
     */
    public void clearSelection() {
        List<Integer> selection = getSelectedItems();
        selectedItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }//clearSelection

    public int getSelectedItemCount() {
        return selectedItems.size();
    }//getSelectedItemCount

    public int getSwipedItem(){
        return swipedItem.first;
    }//getSwipedItem

    public int getSwipedItemDirection(){
        return swipedItem.second;
    }//getSwipedItemDirection

    public int getSelectedItem(){
        return selectedItem;
    }//getSelectedItem

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); ++i) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }//getSelectedItems

    public int[] getSelectedIndexArray() {
        int[] result;
        if (getState() == SINGLE_SWIPE_MODE) {
            result = new int[1];
            result[0] = getSwipedItem();
        } else {
            result = new int[selectedItems.size()];
            int resultIndex = 0;
            for (int i = 0; i < selectedItems.size(); ++i) {
                result[resultIndex++] = selectedItems.keyAt(i);
            }
        }
        Log.d("db","getSelectedIndexArray ");
        return result;
    }

    public Bundle saveSelectionsState(){
        Log.d("db","save selection state");
        Bundle information = new Bundle();
        information.putIntegerArrayList(MULTI_SELECTION_POSITIONS, (ArrayList<Integer>) getSelectedItems());
        information.putInt(SINGLE_SELECTION_POSITION, getSelectedItem());
        information.putInt(SELECTIONS_STATE, getState());
        return information;
    }//saveSelectionsState

    public void restoreSelectionsState(Bundle savedStates){
        Log.d("db","restore selection state");
        int singleSelectedPosition = savedStates.getInt(SINGLE_SELECTION_POSITION);
        List<Integer> selectedPositions = savedStates.getIntegerArrayList(MULTI_SELECTION_POSITIONS);
        selectedItem = singleSelectedPosition;
        restoreSingleSelection();
        restoreSelections(selectedPositions);
        state = savedStates.getInt(SELECTIONS_STATE);
    }//restoreSelectionsState

    private void restoreSelections(List<Integer> selected) {
        if (selected == null) return;
        selectedItems.clear();
        for (int i = 0; i < selected.size(); i++) {
            selectedItems.put(selected.get(i), true);
        }
        notifyDataSetChanged();
    }//restoreSelections

}
