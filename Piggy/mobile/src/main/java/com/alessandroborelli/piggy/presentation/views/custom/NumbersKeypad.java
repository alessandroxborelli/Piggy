package com.alessandroborelli.piggy.presentation.views.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;

/**
 * Created by aborelli on 06/09/2016.
 */
public class NumbersKeypad extends RelativeLayout {

    private static int[] mNumbersBTResId = {
            R.id.keypad_number_0_BT,
            R.id.keypad_number_1_BT,
            R.id.keypad_number_2_BT,
            R.id.keypad_number_3_BT,
            R.id.keypad_number_4_BT,
            R.id.keypad_number_5_BT,
            R.id.keypad_number_6_BT,
            R.id.keypad_number_7_BT,
            R.id.keypad_number_8_BT,
            R.id.keypad_number_9_BT,
            R.id.keypad_decimal_BT
    };

    private OnKeypadClickListener mListener;

    private String currentTypedText, prevTypedText;

    public NumbersKeypad(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Inflate the layout
        LayoutInflater.from(context).inflate(R.layout.view_keypad_numbers, this, true);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setupKeypad();
    }

    private void setupKeypad() {
        // setup number buttons
        for (int id : mNumbersBTResId) {
            Button bt = (Button) this.findViewById(id);
            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View aClickedView) {
                    String typedText = ((TextView)aClickedView).getText().toString();
                    if(!isTypedInputValid(typedText)) return;
                    if(mListener != null) {
                        onKeyNumberClick(typedText);
                        performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                        mListener.onUpdateText(currentTypedText, isTextValid());
                    }
                    prevTypedText = typedText;
                }
            });
        }
        View backspace = this.findViewById(R.id.keypad_backspace_BT);
        backspace.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                onBackspaceClick();
                performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                if(mListener != null) mListener.onUpdateText(currentTypedText, isTextValid());
            }
        });
        backspace.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onBackspaceLongClick();
                performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                if(mListener != null) mListener.onUpdateText(currentTypedText, isTextValid());
                return true;
            }
        });
    }

    public void onKeyNumberClick(String typedNumber) {
        currentTypedText += typedNumber;
    }

    public void onBackspaceClick() {
        prevTypedText = null;
        if(currentTypedText.length() > 0 && !currentTypedText.equals("")){
            currentTypedText = currentTypedText.substring(0, currentTypedText.length() - 1);
        }
    }

    public void onBackspaceLongClick() {
        prevTypedText = null;
        currentTypedText = "";
    }

    public boolean isTextValid() {
        if (currentTypedText.isEmpty()) return false;
        if (currentTypedText.equals("0")) return false;
        if (currentTypedText.equals("0.0")) return false;
        if (currentTypedText.equals("0.00")) return false;
        if (currentTypedText.endsWith(".")) return false;
        return true;
    }

    public boolean isTypedInputValid(String typedText) {
        // Prevent to insert more then two decimal values
        String[] textWithDecimal = (currentTypedText != null && !currentTypedText.isEmpty()) ? currentTypedText.split("\\.") : null;
        if (textWithDecimal != null && textWithDecimal.length > 1) {
            if (textWithDecimal[1].length() == 2) return false;
        }
        // Prevent to insert a different value from . if the first value inserted is 0
        if (currentTypedText.equals("0") && !typedText.equals(".")) return false;
        // Prevent to insert twice the same text
        if (typedText.equals(".")) {
            if (currentTypedText.contains(".")) return false;
            if (currentTypedText.equals("")) return false;
            if (prevTypedText != null && !prevTypedText.isEmpty() && prevTypedText.equals(typedText)) return false;
        }
        return true;
    }

    public void setOnKeypadListener(OnKeypadClickListener listener) {
        mListener = listener;
    }

    public void resetUI() {
        prevTypedText = "";
        currentTypedText = "";
        if(mListener != null) mListener.onUpdateText(currentTypedText, false);
    }

    interface OnKeypadClickListener {
        void onUpdateText(String typedText, boolean isTextValid);
    }

}
