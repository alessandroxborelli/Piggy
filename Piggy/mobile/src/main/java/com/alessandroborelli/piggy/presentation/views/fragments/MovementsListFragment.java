package com.alessandroborelli.piggy.presentation.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.DummyContent;
import com.alessandroborelli.piggy.domain.models.Movement;
import com.alessandroborelli.piggy.presentation.interactors.MovementInteractor;
import com.alessandroborelli.piggy.presentation.views.activities.MainActivity;
import com.alessandroborelli.piggy.presentation.views.activities.PiggyActivity;
import com.alessandroborelli.piggy.presentation.views.adapters.MovementAdapter;
import com.alessandroborelli.piggy.presentation.views.custom.DividerItemDecoration;
import com.alessandroborelli.piggy.presentation.views.custom.EndlessScroll;
import com.alessandroborelli.piggy.presentation.views.custom.SimpleItemTouchHelperCallback;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.Notice;
import com.alessandroborelli.piggy.util.NumberUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by aborelli on 05/09/2016.
 */
public class MovementsListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, EndlessScroll.OnEndlessScrollListener, MovementAdapter.OnItemListener, ChildEventListener {

    public static final String TAG = "MovementsListFragment_TAG";

    private EndlessScroll mEndlessScroll;
    private SwipeRefreshLayout mSwipeContainer;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private MovementAdapter mAdapter;

    private DatabaseReference mMovementsRef;

    private boolean mayAddAtFirst = false;

    public MovementsListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // if you want modify the toolbar from the fragment child
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movements_list, container, false);

        mSwipeContainer = (SwipeRefreshLayout)rootView.findViewById(R.id.movements_swipeRefresh_layout);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setEnabled(false);
        mSwipeContainer.setRefreshing(false);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());

        mAdapter = new MovementAdapter();
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.movements_recyclerView);
        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            //this disables the default item animator
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mEndlessScroll = new EndlessScroll(mLinearLayoutManager) {};
        mEndlessScroll.setOnEndlessScrollListener(this);
        mRecyclerView.setOnScrollListener(mEndlessScroll);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mRecyclerView);

        mAdapter.setOnItemClickListener(this);

        String movementsUrl = Constants.getFirebaseMovementsUrl(((PiggyActivity)getActivity()).getCurrentPiggyKey());
        mMovementsRef = ((MainActivity)getActivity()).getFirebaseDatabase().getReferenceFromUrl(movementsUrl);
        mMovementsRef.orderByChild("timestamp").addChildEventListener(this);

        return rootView;
    }


    /**
     * SwipeRefreshLayout.OnRefreshListener
     */
    @Override
    public void onRefresh() {
        // simulate the delay of request, so after 2 secs hide the icon refreshing icon
        mSwipeContainer.postDelayed(new Runnable() {
            public void run() {
                mSwipeContainer.setRefreshing(false);
                DummyContent.ITEMS.clear();
                DummyContent.loadMore();
                mAdapter = new MovementAdapter();
                mRecyclerView.setAdapter(mAdapter);
            }
        }, 2000);
    }

    /**
     * EndlessScroll.OnEndlessScrollListener
     * @param currentPage
     */
    @Override
    public void onLoadMore(int currentPage) {
        DummyContent.ITEMS.add(null);
        mAdapter.notifyItemInserted(DummyContent.ITEMS.size() - 1);
        Log.d("db", "count "+mAdapter.getItemCount());
        // simulate the delay of request, so after 2 secs hide the icon refreshing icon
        mRecyclerView.postDelayed(new Runnable() {
            public void run() {
                //remove progress item
                DummyContent.ITEMS.remove(DummyContent.ITEMS.size() - 1);
                mAdapter.notifyItemRemoved(DummyContent.ITEMS.size());
                DummyContent.loadMore();
                mAdapter.notifyDataSetChanged();
            }
        }, 2000);
    }

    @Override
    public void onScrollStateChanged(int newState) {

    }

    /**
     * Adapter.OnItemClickListener
     * @param itemView
     * @param pos
     */
    @Override
    public void onItemClick(View itemView, int pos) {
        mAdapter.singleSelection(pos);
    }

    @Override
    public boolean onItemLongClick(View itemView, int pos) {
        return false;
    }

    @Override
    public void onItemSwiped(Movement movement, String key) {
        MovementInteractor.remove(movement, key, ((PiggyActivity)getActivity()).getCurrentPiggyKey());
    }

    public void addDummyMovement() {
        String currentDateTimeString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date());
        double randomAmount = Math.random()*1000;
        randomAmount = NumberUtils.getScaledDouble(randomAmount);
        Movement movement = new Movement(randomAmount, 2, currentDateTimeString, "description", 2);
        MovementInteractor.add(movement, ((PiggyActivity)getActivity()).getCurrentPiggyKey());
        mayAddAtFirst = true;
    }

    public void refreshDatabaseReference() {
        String movementsUrl = Constants.getFirebaseMovementsUrl(((PiggyActivity)getActivity()).getCurrentPiggyKey());
        mMovementsRef = ((MainActivity)getActivity()).getFirebaseDatabase().getReferenceFromUrl(movementsUrl);
        mMovementsRef.orderByChild("timestamp").addChildEventListener(this);
    }

    /**
     * ChildEventListener
     * @param dataSnapshot
     * @param s
     */

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Movement movement = dataSnapshot.getValue(Movement.class);
        Notice.debug(getClass(), "onChildAdded() -> "+movement.toString());
        String key = dataSnapshot.getKey();
        if (mayAddAtFirst) {
            mayAddAtFirst = false;
            mAdapter.addAtFirst(movement, key);
        } else {
            mAdapter.add(movement, key);
        }
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Movement movement = dataSnapshot.getValue(Movement.class);
        Notice.debug(getClass(), "onChildChanged() -> "+dataSnapshot.toString());
        String key = dataSnapshot.getKey();
        mAdapter.update(movement, key);
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Notice.debug(getClass(), "onChildChanged() -> "+dataSnapshot.toString());
        String key = dataSnapshot.getKey();
        mAdapter.remove(key);
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
