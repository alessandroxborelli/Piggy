package com.alessandroborelli.piggy;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

/**
 * Created by alessandroborelli on 13/07/16.
 */

public class MApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
