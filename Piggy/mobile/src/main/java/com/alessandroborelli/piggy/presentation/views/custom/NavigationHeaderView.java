package com.alessandroborelli.piggy.presentation.views.custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.support.design.internal.NavigationMenuView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.presentation.views.adapters.PiggyAdapter;
import com.alessandroborelli.piggy.util.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by aborelli on 19/08/2016.
 */
public class NavigationHeaderView extends LinearLayout {

    // The parent of this view (for removing and adding itself)
    private ViewGroup mParent;

    // RecyclerView used in NavigationView populate by xml list
    private NavigationMenuView mNavigationMenuView;
    private RecyclerView.Adapter mMenuAdapter;
    private PiggyAdapter mPiggyAdapter;

    private OnPiggySelectedListener mListener;

    private View mDropDownContainerView;
    private CircularImageView mAccountImageCIView;
    private TextView mAccountNameTView;
    private ImageView mDropDownIView;
    private TextView mItemNameTView;
    private TextView mItemDetailTView;

    private boolean mIsShowingSecondaryList;

    public NavigationHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Provide default background if none is set
        if (getBackground() == null) {
            //setBackground(getResources().getDrawable(R.drawable.side_nav_bar));
            setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Retrieve the style attributes
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NavigationHeaderView, defStyleAttr, 0);
        boolean mAddEnabled = a.getBoolean(R.styleable.NavigationHeaderView_AddEnabled, true);
        boolean mManageEnabled = a.getBoolean(R.styleable.NavigationHeaderView_ManageEnabled, true);
        a.recycle();

        // Inflate the layout
        LayoutInflater.from(context).inflate(R.layout.nav_header_piggy, this, true);

        mDropDownIView = (ImageView) findViewById(R.id.nav_header_drop_drown_BT);
        mDropDownContainerView = findViewById(R.id.nav_header_drop_drown_container_BT);
        mDropDownContainerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mIsShowingSecondaryList) {
                    showMainList();
                } else {
                    showSecondaryList();
                }
            }
        });

        mAccountNameTView = (TextView) findViewById(R.id.nav_header_account_name_TV);
        mItemNameTView = (TextView) findViewById(R.id.nav_header_item_name_TV);
        mItemDetailTView = (TextView) findViewById(R.id.nav_header_item_detail_TV);
        mAccountImageCIView = (CircularImageView) findViewById(R.id.nav_header_account_image_IV);

        mPiggyAdapter = new PiggyAdapter(new ArrayList<Piggy>(), this);
    }

    public NavigationHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationHeaderView(Context context) {
        this(context, null);
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w > 0 && h > 0 && mNavigationMenuView == null)
            mNavigationMenuView = (NavigationMenuView) getParent().getParent();
    }


    private void removeViewFromParent() {
        getViewParent().removeView(this);
    }

    private void addViewToParent() {
        getViewParent().addView(this);
    }

    private ViewGroup getViewParent() {
        if (mParent == null) {
            mParent = (ViewGroup) getParent();
        }
        return mParent;
    }

    public boolean isShowingSecondaryList() {
        return mIsShowingSecondaryList;
    }

    public void showMainList() {
        Log.d("db","show main list");
        if (mIsShowingSecondaryList) {
            mDropDownIView.animate().rotation(0).setDuration(Constants.ANIM_DURATION_DROP_DOWN).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    // reset previously cached menu adapter
                    mNavigationMenuView.setAdapter(mMenuAdapter);
                    mMenuAdapter = null;
                    addViewToParent();
                }
            });
        }
        mIsShowingSecondaryList = false;
    }

    public void showSecondaryList() {
        Log.d("db","show secondary list");
        if (!mIsShowingSecondaryList) {
            mDropDownIView.animate().rotation(180).setDuration(Constants.ANIM_DURATION_DROP_DOWN).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    // cache menu adapter for later
                    removeViewFromParent();
                    mMenuAdapter = mNavigationMenuView.getAdapter();
                    mNavigationMenuView.setAdapter(mPiggyAdapter);
                }
            });
        }
        mIsShowingSecondaryList = true;
    }

    public void setAccountName(String aText) {
        mAccountNameTView.setText(aText);
    }

    public void setAccountImage(Context context, Uri photoUrl) {
        // Set the placeholder
        mAccountImageCIView.setImageResource(R.drawable.placeholder_person);
        if(photoUrl != null) Picasso.with(context).load(photoUrl).into(mAccountImageCIView);
    }

    public void setItemName(String aText) {
        mItemNameTView.setText(aText);
    }

    public void setItemDetail(String aText) {
        mItemDetailTView.setText(aText);
    }


    // Handles an account click passed on by the account list adapter
    public void handlePiggyClick(int position) {
        showMainList();
        int count = mPiggyAdapter.getItemCount();
        if (position < mPiggyAdapter.getListCount()) {
            Piggy selectedPiggy = mPiggyAdapter.get(position);
            boolean makePrimary = true;
            if (mListener != null) makePrimary = mListener.onPiggySelected(selectedPiggy);
            if (makePrimary) switchPrimaryPiggy(selectedPiggy);
        } else {
            if (mListener != null) mListener.onAddNewPiggy();
        }
    }

    // Handles an account click passed on by the account list adapter
    public void handlePiggyOptionClick(View view, int position) {
        Piggy selectedPiggy = mPiggyAdapter.get(position);
        if (mListener != null) mListener.onEditPiggySelected(view, selectedPiggy);
    }

    private void switchPrimaryPiggy(Piggy newPrimaryPiggy) {
        mPiggyAdapter.move(newPrimaryPiggy, 0);
        updatePiggyHeader();
    }

    public void updatePiggyHeader() {
        // Set the piggy header getting the first element in the list
        if(mPiggyAdapter != null && mPiggyAdapter.getListCount() > 0) {
            Piggy mainPiggy = mPiggyAdapter.get(0);
            setItemName(mainPiggy.getName());
            setItemDetail(mainPiggy.getDisplayableIncome());
        }
    }

    public void addPiggies(Piggy... piggies) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            for (Piggy piggy : piggies) {
                mPiggyAdapter.add(piggy);
            }
        } else {
            mPiggyAdapter.addAll(piggies);
        }
        updatePiggyHeader();
    }

    public void addPiggy(Piggy aPiggy) {
        if (mPiggyAdapter.add(aPiggy)) updatePiggyHeader();
    }

    public void removePiggy(String key) {
        if (mPiggyAdapter.remove(key)) updatePiggyHeader();
    }

    public void insertPiggy(Piggy aPiggy, int position) {
        mPiggyAdapter.insert(aPiggy, position);
        updatePiggyHeader();
    }

    public void clearPiggies() {
        if(mPiggyAdapter != null && mPiggyAdapter.getListCount() > 0) {
            mPiggyAdapter.clear();
            updatePiggyHeader();
        }
    }

    public void setPiggySelectedListener(OnPiggySelectedListener listener) {
        mListener = listener;
    }
    /**
     * Listener for handling events on piggy list
     */
    public interface OnPiggySelectedListener {

        /**
         * Called when an account in the account header is selected.
         *
         * @param piggy The selected account
         *
         * @return true to display the selected piggy as the primary piggy
         */
        boolean onPiggySelected(Piggy piggy);

        /**
         * Called when the tree dot icon item is selected.
         */
        void onEditPiggySelected(View view, Piggy piggy);

        /**
         * Called when the "Add piggy" item is selected.
         */
        void onAddNewPiggy();

    }

    /**
     * Adapter class for listener
     */
    public static class OnPiggySelectedListenerAdapter implements OnPiggySelectedListener{

        @Override
        public boolean onPiggySelected(Piggy piggy) {
            return true;
        }

        @Override
        public void onEditPiggySelected(View view, Piggy piggy) {

        }

        @Override
        public void onAddNewPiggy() {

        }
    }

}
