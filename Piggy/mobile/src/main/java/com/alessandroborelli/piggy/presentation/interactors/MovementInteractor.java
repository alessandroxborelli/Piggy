package com.alessandroborelli.piggy.presentation.interactors;

import com.alessandroborelli.piggy.domain.models.Movement;
import com.alessandroborelli.piggy.domain.models.Piggy;
import com.alessandroborelli.piggy.util.Constants;
import com.alessandroborelli.piggy.util.NumberUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.value;

/**
 * Created by alessandroborelli on 23/10/2016.
 */

public class MovementInteractor {

    public static void add(final Movement movement, final String currentPiggyKey) {
        DatabaseReference piggyRef = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_PIGGIES_URL).child(currentPiggyKey);
        piggyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Piggy aPiggy = dataSnapshot.getValue(Piggy.class);
                if (aPiggy != null) {
                    String movementKey = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.getFirebaseMovementsUrl(currentPiggyKey)).push().getKey();
                    double updatedIncome = NumberUtils.getScaledDouble(aPiggy.income + movement.amount);
                    Map<String, Object> movementValues = movement.toMap();

                    Map<String, Object> childUpdates = new HashMap<>();
                    // Add new movement to movements
                    childUpdates.put("/movements/" + currentPiggyKey + "/" +movementKey, movementValues);
                    // Update income value for the current piggy
                    childUpdates.put("/piggies/" + currentPiggyKey + "/income", updatedIncome);
                    FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void remove(final Movement movement, final String movementKey, final String currentPiggyKey) {
        DatabaseReference piggyRef = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_PIGGIES_URL).child(currentPiggyKey);
        piggyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Piggy aPiggy = dataSnapshot.getValue(Piggy.class);
                if (aPiggy != null) {
                    double updatedIncome = NumberUtils.getScaledDouble(aPiggy.income - movement.amount);
                    Map<String, Object> childUpdates = new HashMap<>();
                    // Add new movement to movements
                    childUpdates.put("/movements/" + currentPiggyKey + "/" +movementKey, null);
                    // Update income value for the current piggy
                    childUpdates.put("/piggies/" + currentPiggyKey + "/income", updatedIncome);
                    FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
