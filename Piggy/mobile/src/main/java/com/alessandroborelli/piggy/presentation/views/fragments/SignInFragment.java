package com.alessandroborelli.piggy.presentation.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.alessandroborelli.piggy.R;
import com.alessandroborelli.piggy.presentation.interactors.AuthInteractor;
import com.alessandroborelli.piggy.presentation.presenters.AuthPresenter;
import com.alessandroborelli.piggy.presentation.presenters.IAuthVPI;
import com.alessandroborelli.piggy.presentation.views.activities.AuthenticationActivity;
import com.alessandroborelli.piggy.presentation.views.activities.MainActivity;
import com.alessandroborelli.piggy.util.BundleKeys;
import com.alessandroborelli.piggy.util.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by alessandroborelli on 17/08/16.
 */

public class SignInFragment extends Fragment implements IAuthVPI.RequiredViewActions, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = "SignInFragment";

    private IAuthVPI.PresenterActions mPresenter;
    private GoogleSignInOptions mGso;
    private GoogleApiClient mGoogleApiClient;

    private TextInputLayout mEmailInputLayout;
    private TextInputLayout mPasswordInputLayout;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private Button mSignInButton;
    private Button mJoinUsButton;
    private SignInButton mSignInGoogleButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (savedInstanceState == null) {
            // Creates presenter
            mPresenter = new AuthPresenter(this, getActivity());
            mPresenter.onCreate();
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            mGso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.google_server_client_id))
                    .requestEmail()
                    .build();
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage(getActivity(), this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, mGso)
                    .build();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_auth_sign_in, container, false);
        mEmailInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_signIn_layout_email);
        mPasswordInputLayout = (TextInputLayout) rootView.findViewById(R.id.auth_signIn_layout_password);
        mEmailEditText = (EditText) rootView.findViewById(R.id.auth_signIn_email_ET);
        mPasswordEditText = (EditText) rootView.findViewById(R.id.auth_signIn_password_ET);
        mSignInButton = (Button) rootView.findViewById(R.id.auth_signIn_submit_BT);
        mJoinUsButton = (Button) rootView.findViewById(R.id.auth_signIn_join_BT);
        mSignInGoogleButton = (SignInButton) rootView.findViewById(R.id.auth_signIn_google_BT);
        mSignInButton.setOnClickListener(this);
        mJoinUsButton.setOnClickListener(this);
        mSignInGoogleButton.setOnClickListener(this);
        mEmailEditText.addTextChangedListener(new AuthFormTextWatcher(mEmailEditText));
        mPasswordEditText.addTextChangedListener(new AuthFormTextWatcher(mPasswordEditText));
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    public void goBackToJoinUs() {
        ((AuthenticationActivity)getActivity()).showJoinUs(true);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == mSignInButton.getId()) {
            // Tries to authenticate
            String typedEmail = mEmailEditText.getText().toString().trim();
            String typedPassword = mPasswordEditText.getText().toString();
            mPresenter.requestAuthentication(typedEmail, typedPassword);
        } else if (id == mJoinUsButton.getId()) {
            // Back to Join fragment
            goBackToJoinUs();
        } else if (mSignInGoogleButton.getId() == id) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            getActivity().startActivityForResult(signInIntent, Constants.REQUEST_CODE_GOOGLE_SIGN_IN);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    /**
     * PRESENTER  ->  VIEW
     */

    @Override
    public void goToHome() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra(BundleKeys.IS_USER_SIGNED_IN, true);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void updateUI(AuthInteractor.Stage aStage) {
        switch (aStage) {
            case EMAIL_VALID:
                mEmailInputLayout.setError(null);
                mEmailInputLayout.setErrorEnabled(false);
                break;
            case PASSWORD_VALID:
                mPasswordInputLayout.setError(null);
                mPasswordInputLayout.setErrorEnabled(false);
                break;
        }
    }

    @Override
    public void showError(int errorId, String msg) {
        switch (errorId) {
            case Constants.ERROR_AUTH_EMAIL_NOT_VALID:
                mEmailInputLayout.setErrorEnabled(true);
                mEmailInputLayout.setError(getString(R.string.error_enter_email));
                requestFocus(mEmailEditText);
                break;
            case Constants.ERROR_AUTH_PASSWORD_NOT_VALID:
                mPasswordInputLayout.setErrorEnabled(true);
                mPasswordInputLayout.setError(getString(R.string.error_enter_password));
                requestFocus(mPasswordEditText);
                break;
        }
    }

    /**
     * END
     */

    /**
     * GoogleApiClient OnConnectionFailedListener
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class AuthFormTextWatcher implements TextWatcher {

        private View view;

        private AuthFormTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.auth_signIn_email_ET:
                    if (AuthInteractor.isValidEmail(editable.toString())) {
                        Log.d(Constants.TAG,"email VALID");
                        mEmailInputLayout.setError(null);
                        mEmailInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"email NOT VALID");
                        mEmailInputLayout.setErrorEnabled(true);
                        mEmailInputLayout.setError(getString(R.string.error_enter_email));
                        requestFocus(mEmailEditText);
                    }
                    break;
                case R.id.auth_signIn_password_ET:
                    if (AuthInteractor.isValidPassword(editable.toString())) {
                        Log.d(Constants.TAG,"password VALID");
                        mPasswordInputLayout.setError(null);
                        mPasswordInputLayout.setErrorEnabled(false);
                    } else {
                        Log.d(Constants.TAG,"password NOT VALID");
                        mPasswordInputLayout.setErrorEnabled(true);
                        mPasswordInputLayout.setError(getString(R.string.error_enter_password));
                        requestFocus(mPasswordEditText);
                    }
                    break;
            }
        }
    }

}
